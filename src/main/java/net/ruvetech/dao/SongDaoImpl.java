package net.ruvetech.dao;

import net.ruvetech.data.Song;
import com.googlecode.ehcache.annotations.Cacheable;

public class SongDaoImpl implements SongDao {

    @Cacheable(cacheName = "findSong", keyGeneratorName = "methodHashCodeKeyGenerator")
    @Override
    public Song findSong(String title, String singer) {
        slowQuery(2);
        System.out.println("finding song By singer...");
        return new Song("This is the end..","Adele ",4.5f);
    }

    private void slowQuery(long seconds) {
        try {
            Thread.sleep(seconds);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
