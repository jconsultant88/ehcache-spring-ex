package net.ruvetech.dao;

import org.springframework.cache.annotation.Cacheable;
import net.ruvetech.data.Book;

public class BookDaoImpl implements BookDao {

    @Cacheable(value = "bookCache", key = "#author.concat('-').concat(#name)")
    public Book findBook(String author, String name) {
        slowQuery(2);
        System.out.println("finding Book By Author...");
        return new Book("Killing a mocking bird","Harper Lee",4);
    }

    private void slowQuery(long seconds) {
        try {
            Thread.sleep(seconds);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
