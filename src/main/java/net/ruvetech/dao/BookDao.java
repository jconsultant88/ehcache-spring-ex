package net.ruvetech.dao;

import net.ruvetech.data.Book;

public interface BookDao {

    public Book findBook(String author, String name);
}
