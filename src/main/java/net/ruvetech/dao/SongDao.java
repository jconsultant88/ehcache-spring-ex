package net.ruvetech.dao;

import net.ruvetech.data.Song;

public interface SongDao {

    public Song findSong(String title, String author);
}
