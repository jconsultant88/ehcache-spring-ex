package net.ruvetech;

import java.util.Collection;
import java.util.LinkedHashSet;

import org.springframework.cache.Cache;
import org.springframework.cache.ehcache.EhCacheCache;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ruvetech.dao.BookDao;
import net.ruvetech.dao.SongDao;
import net.sf.ehcache.CacheManager;

public class SpringEhCacheEx {
    private static final Logger log = LoggerFactory.getLogger(SpringEhCacheEx.class);

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");

        EhCacheCacheManager ehCacheCacheManager = (EhCacheCacheManager) context.getBean("springMatmCommonEhcacheManager");

        CacheManager cacheManager = ehCacheCacheManager.getCacheManager();

        System.out.println("cacheManager Name: " + cacheManager.getName());

        String[] names = cacheManager.getCacheNames();

        for (int i$ = 0; i$ < names.length; ++i$) {
            String name = names[i$];
            System.out.println("Cache Name: " + name);
            net.sf.ehcache.Cache cache = cacheManager.getCache(name);
            System.out.println(cache);
        }

        BookDao bookDao = (BookDao) context.getBean("bookDao");

        log.debug("Book Result : {}", bookDao.findBook("dummyAuthor", "dummyName"));
        log.debug("Book Result : {}", bookDao.findBook("dummyAuthor", "dummyName"));
        log.debug("Book Result : {}", bookDao.findBook("dummyAuthor", "dummyName"));

        SongDao songDao = (SongDao) context.getBean("songDao");

        log.debug("Song Result : {}", songDao.findSong("dummyTitle", "dummyAuthor"));
        log.debug("Song Result : {}", songDao.findSong("dummyTitle", "dummyAuthor"));
        log.debug("Song Result : {}", songDao.findSong("dummyTitle", "dummyAuthor"));


        //shut down the Spring context.
        ((ClassPathXmlApplicationContext) context).close();
    }
}
