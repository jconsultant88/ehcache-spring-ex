package net.ruvetech.data;

import java.util.StringJoiner;

public class Song {
    
    private String title;
    private String singer;
    private float duration;

    public Song() {
    }

    public Song(String title, String singer, float duration) {
        this.title = title;
        this.singer = singer;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }


    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(", ");
        sj.add(title);
        sj.add(singer);
        sj.add("" + duration);
        return sj.toString();
    }
}
